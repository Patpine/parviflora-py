import unittest
import binformat
from io import BytesIO


class TestBinformat(unittest.TestCase):
    def test_int(self):
        buffer = BytesIO()
        binformat.write_msg(buffer, 420)

        self.assertEqual(buffer.getvalue(), b'\x01\xa4\x01\x00\x00\x00\x00\x00\x00')
    
    def test_float(self):
        buffer = BytesIO()
        binformat.write_msg(buffer, 3.1415)

        self.assertEqual(buffer.getvalue(), b'\x02o\x12\x83\xc0\xca!\t@')
    
    def test_bytes(self):
        buffer = BytesIO()
        binformat.write_msg(buffer, b"hello world")

        self.assertEqual(buffer.getvalue(), b'\x03\x0b\x00\x00\x00\x00\x00\x00\x00hello world')
    
    def test_list(self):
        buffer = BytesIO()
        binformat.write_msg(buffer, [1, 2, 3])

        self.assertEqual(buffer.getvalue(), b'\x05\x03\x00\x00\x00\x00\x00\x00\x00\x01\x01\x00\x00\x00\x00\x00\x00\x00\x02\x00\x00\x00\x00\x00\x00\x00\x03\x00\x00\x00\x00\x00\x00\x00')
    
    def test_object(self):
        buffer = BytesIO()
        binformat.write_msg(buffer, {1: 1, 2: [1, 2]})

        self.assertEqual(buffer.getvalue(), b'\x00\x02\x00\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x01\x01\x00\x00\x00\x00\x00\x00\x00\x02\x00\x00\x00\x00\x00\x00\x00\x05\x02\x00\x00\x00\x00\x00\x00\x00\x01\x01\x00\x00\x00\x00\x00\x00\x00\x02\x00\x00\x00\x00\x00\x00\x00')

class TestSafeMethods(unittest.TestCase):
    def test_safe_write(self):
        ret = binformat.safe_write_msg("should_not_exist", [1, 2, 3.5])
        with self.assertRaises(Exception):
            open("should_not_exist", "rb")
        
        self.assertEqual(ret, "Error: All items in array must be the same, and a type representable by binformat")

    def test_safe_read(self):
        ret = binformat.safe_read_msg("lollmao")
        
        self.assertEqual(ret, None)


if __name__ == '__main__':
    unittest.main()